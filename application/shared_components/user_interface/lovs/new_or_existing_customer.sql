prompt --application/shared_components/user_interface/lovs/new_or_existing_customer
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(4120384427467174578)
,p_lov_name=>'NEW OR EXISTING CUSTOMER'
,p_lov_query=>'.'||wwv_flow_api.id(4120384427467174578)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(4120384644704174581)
,p_lov_disp_sequence=>10
,p_lov_disp_value=>'Existing customer'
,p_lov_return_value=>'EXISTING'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(4120384858795174586)
,p_lov_disp_sequence=>20
,p_lov_disp_value=>'New customer'
,p_lov_return_value=>'NEW'
);
end;
/
