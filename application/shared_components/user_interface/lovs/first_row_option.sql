prompt --application/shared_components/user_interface/lovs/first_row_option
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(7660518403927112402)
,p_lov_name=>'FIRST_ROW_OPTION'
,p_lov_query=>'.'||wwv_flow_api.id(7660518403927112402)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(7660518687374112402)
,p_lov_disp_sequence=>10
,p_lov_disp_value=>'Yes'
,p_lov_return_value=>'Y'
);
end;
/
