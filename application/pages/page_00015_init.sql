prompt --application/set_environment
set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_190100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2019.03.31'
,p_release=>'19.1.0.00.15'
,p_default_workspace_id=>1260514089020361
,p_default_application_id=>100
,p_default_owner=>'DEV'
);
end;
/
 
prompt APPLICATION 100 - Sample Database Application
--
-- Application Export:
--   Application:     100
--   Name:            Sample Database Application
--   Date and Time:   04:58 Monday January 13, 2020
--   Exported By:     3.91.43.76
--   Flashback:       0
--   Export Type:     Page Export
--   Version:         19.1.0.00.15
--   Instance ID:     250157796364266
--

prompt --application/pages/delete_00015
begin
wwv_flow_api.remove_page (p_flow_id=>wwv_flow.g_flow_id, p_page_id=>15);
end;
/
