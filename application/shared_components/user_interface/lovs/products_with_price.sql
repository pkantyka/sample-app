prompt --application/shared_components/user_interface/lovs/products_with_price
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(7500590535400297007)
,p_lov_name=>'PRODUCTS WITH PRICE'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select apex_escape.html(product_name) || '' [$'' || list_price || '']'' d, product_id r from demo_product_info',
'where product_avail = ''Y''',
'order by 1'))
);
end;
/
