prompt --application/pages/page_00027
begin
wwv_flow_api.create_page(
 p_id=>27
,p_user_interface_id=>wwv_flow_api.id(1524532408908590285)
,p_tab_set=>'TS1'
,p_name=>'Sales by Product'
,p_step_title=>'&APP_NAME. - Sales by Product'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_group_id=>wwv_flow_api.id(1683714225797609049)
,p_step_template=>wwv_flow_api.id(1264401211016662423)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_protection_level=>'C'
,p_last_updated_by=>'HILARY'
,p_last_upd_yyyymmddhh24miss=>'20190314064830'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(642844517941561954)
,p_plug_name=>'Sales by Product'
,p_region_template_options=>'#DEFAULT#:t-Region--noPadding:t-Region--hideHeader:t-Region--scrollBody'
,p_escape_on_http_output=>'Y'
,p_plug_template=>wwv_flow_api.id(1264437043388662545)
,p_plug_display_sequence=>10
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''f?p=&APP_ID.:6:''||:app_session||'':::6:P6_PRODUCT_ID,P6_BRANCH:''||p.product_id||'',27:'' link, ',
'    p.product_name||'' [$''||p.list_price||'']'' product,',
'    SUM(oi.quantity * oi.unit_price) sales,',
'    decode(nvl(dbms_lob.getlength(p.product_image),0),0,null,',
'        apex_util.get_blob_file_src(''P6_PRODUCT_IMAGE'',p.product_id)) product_image',
'from demo_order_items oi,',
'    demo_product_info p',
'where oi.product_id = p.product_id',
'group by p.product_id, p.product_name, p.list_price,',
'    decode(nvl(dbms_lob.getlength(p.product_image),0),0,null,',
'        apex_util.get_blob_file_src(''P6_PRODUCT_IMAGE'',p.product_id))',
'order by 3 desc, 1'))
,p_plug_source_type=>'PLUGIN_COM.ORACLE.APEX.HTML5_BAR_CHART'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'IMAGE'
,p_attribute_02=>'PRODUCT'
,p_attribute_03=>'&LINK.'
,p_attribute_04=>'SALES'
,p_attribute_05=>'&LINK.'
,p_attribute_07=>'&PRODUCT_IMAGE.'
,p_attribute_11=>'VALUE'
,p_attribute_14=>'15'
,p_attribute_15=>'ICON'
,p_attribute_16=>'ABSOLUTE'
,p_attribute_17=>'MODERN_2'
,p_attribute_18=>'AROUND'
,p_attribute_20=>'No data found.'
);
end;
/
