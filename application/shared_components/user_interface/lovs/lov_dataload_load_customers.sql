prompt --application/shared_components/user_interface/lovs/lov_dataload_load_customers
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(939106176805971947)
,p_lov_name=>'LOV_DATALOAD_LOAD CUSTOMERS'
,p_lov_query=>'.'||wwv_flow_api.id(939106176805971947)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(939106872042971948)
,p_lov_disp_sequence=>10
,p_lov_disp_value=>'Customer Identifier'
,p_lov_return_value=>'CUSTOMER_ID'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(939107772222971948)
,p_lov_disp_sequence=>20
,p_lov_disp_value=>'First Name'
,p_lov_return_value=>'CUST_FIRST_NAME'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(939108073260971948)
,p_lov_disp_sequence=>30
,p_lov_disp_value=>'Last Name'
,p_lov_return_value=>'CUST_LAST_NAME'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(939108968469971948)
,p_lov_disp_sequence=>40
,p_lov_disp_value=>'Street Address'
,p_lov_return_value=>'CUST_STREET_ADDRESS1'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(939109258617971949)
,p_lov_disp_sequence=>50
,p_lov_disp_value=>'Street Address Line 2'
,p_lov_return_value=>'CUST_STREET_ADDRESS2'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(939107162624971948)
,p_lov_disp_sequence=>60
,p_lov_disp_value=>'City'
,p_lov_return_value=>'CUST_CITY'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(939108676109971948)
,p_lov_disp_sequence=>70
,p_lov_disp_value=>'State'
,p_lov_return_value=>'CUST_STATE'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(939108372704971948)
,p_lov_disp_sequence=>80
,p_lov_disp_value=>'Postal Code'
,p_lov_return_value=>'CUST_POSTAL_CODE'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(939107490869971948)
,p_lov_disp_sequence=>90
,p_lov_disp_value=>'Email'
,p_lov_return_value=>'CUST_EMAIL'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(939109566731971949)
,p_lov_disp_sequence=>100
,p_lov_disp_value=>'Phone Number'
,p_lov_return_value=>'PHONE_NUMBER1'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(939109872312971950)
,p_lov_disp_sequence=>110
,p_lov_disp_value=>'Alternate Number'
,p_lov_return_value=>'PHONE_NUMBER2'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(939110473285971950)
,p_lov_disp_sequence=>120
,p_lov_disp_value=>'URL'
,p_lov_return_value=>'URL'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(939106560587971948)
,p_lov_disp_sequence=>130
,p_lov_disp_value=>'Credit Limit'
,p_lov_return_value=>'CREDIT_LIMIT'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(939110161050971950)
,p_lov_disp_sequence=>140
,p_lov_disp_value=>'Tags'
,p_lov_return_value=>'TAGS'
);
end;
/
