prompt --application/deployment/install/upgrade_sample_pkg_spec
begin
wwv_flow_api.create_install_script(
 p_id=>wwv_flow_api.id(4120411355107970031)
,p_install_id=>wwv_flow_api.id(7182105393450777194)
,p_name=>'sample_pkg spec'
,p_sequence=>20
,p_script_type=>'UPGRADE'
,p_script_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'create or replace package sample_pkg is',
'    --',
'    -- Error Handling function',
'    --',
'    function demo_error_handling (',
'        p_error in apex_error.t_error )',
'        return apex_error.t_error_result;',
'    ',
'    --',
'    -- Tag Cleaner function',
'    --',
'    function demo_tags_cleaner (',
'        p_tags  in varchar2,',
'        p_case  in varchar2 default ''U'')',
'        return varchar2;',
'    ',
'    --',
'    -- Tag Synchronisation Procedure',
'    --',
'    procedure demo_tag_sync (',
'        p_new_tags          in varchar2,',
'        p_old_tags          in varchar2,',
'        p_content_type      in varchar2,',
'        p_content_id        in number );',
'',
'end sample_pkg;'))
);
wwv_flow_api.create_install_object(
 p_id=>wwv_flow_api.id(1670657988706251851)
,p_script_id=>wwv_flow_api.id(4120411355107970031)
,p_object_owner=>'#OWNER#'
,p_object_type=>'PACKAGE'
,p_object_name=>'SAMPLE_PKG'
,p_last_updated_on=>to_date('20141219062049','YYYYMMDDHH24MISS')
,p_created_on=>to_date('20141219062049','YYYYMMDDHH24MISS')
);
end;
/
