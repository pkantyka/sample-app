prompt --application/shared_components/navigation/lists/order_wizard
begin
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(7492749032095951689)
,p_name=>'Order Wizard'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(7492749437982953359)
,p_list_item_display_sequence=>10
,p_list_item_link_text=>'Identify Customer'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'11'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(404244242198620062)
,p_list_item_display_sequence=>20
,p_list_item_link_text=>'Order Items'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'12'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(7500653633264892473)
,p_list_item_display_sequence=>30
,p_list_item_link_text=>'Order Summary'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'14'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(693241300829956267)
,p_list_item_display_sequence=>40
,p_list_item_link_text=>'Order Confirmation'
,p_list_item_current_for_pages=>'8'
);
end;
/
