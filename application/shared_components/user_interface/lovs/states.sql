prompt --application/shared_components/user_interface/lovs/states
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(7476993807632182719)
,p_lov_name=>'STATES'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select initcap(state_name) display_value, st return_value from   demo_states',
'order by 1'))
);
end;
/
