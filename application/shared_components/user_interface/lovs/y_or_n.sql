prompt --application/shared_components/user_interface/lovs/y_or_n
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(7476095131574951102)
,p_lov_name=>'Y OR N'
,p_lov_query=>'.'||wwv_flow_api.id(7476095131574951102)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(7476095422594951105)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'Yes'
,p_lov_return_value=>'Y'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(7476095632315951107)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'No'
,p_lov_return_value=>'N'
);
end;
/
