prompt --application/shared_components/navigation/lists/footer_controls
begin
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(1681935517845214201)
,p_name=>'Footer Controls'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(1681935727988214201)
,p_list_item_display_sequence=>10
,p_list_item_link_text=>'Full Site'
,p_list_item_link_target=>'f?p=&APP_ID.:1:&SESSION.::&DEBUG.::::'
,p_list_text_01=>'star'
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(1681936015358214203)
,p_list_item_display_sequence=>20
,p_list_item_link_text=>'Logout'
,p_list_item_link_target=>'&LOGOUT_URL.'
,p_list_text_01=>'delete'
,p_list_item_current_type=>'TARGET_PAGE'
);
end;
/
