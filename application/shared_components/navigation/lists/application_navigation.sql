prompt --application/shared_components/navigation/lists/application_navigation
begin
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(662321680284811503)
,p_name=>'Application Navigation'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(662321748702811503)
,p_list_item_display_sequence=>10
,p_list_item_link_text=>'Home'
,p_list_item_link_target=>'f?p=&APP_ID.:1:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'fa-home'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'1,15,30'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(662321833459811503)
,p_list_item_display_sequence=>20
,p_list_item_link_text=>'Customers [&A01.]'
,p_list_item_link_target=>'f?p=&APP_ID.:2:&SESSION.::&DEBUG.:RIR:::'
,p_list_item_icon=>'fa-users'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'2,7,21,22,23,24,51,52,53,54'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(662322012880811503)
,p_list_item_display_sequence=>30
,p_list_item_link_text=>'Products [&A02.]'
,p_list_item_link_target=>'f?p=&APP_ID.:3:&SESSION.::&DEBUG.:RIR:::'
,p_list_item_icon=>'fa-shopping-cart'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'3,6'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(662322094996811503)
,p_list_item_display_sequence=>40
,p_list_item_link_text=>'Orders [&A03.]'
,p_list_item_link_target=>'f?p=&APP_ID.:4:&SESSION.::&DEBUG.:RIR:::'
,p_list_item_icon=>'fa-list-alt'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'4,11,12,14,29,14'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(1409116406262817349)
,p_list_item_display_sequence=>50
,p_list_item_link_text=>'Orders Calendar'
,p_list_item_link_target=>'f?p=&APP_ID.:10:&SESSION.::&DEBUG.:RP,10:::'
,p_list_item_disp_cond_type=>'NEVER'
,p_parent_list_item_id=>wwv_flow_api.id(662322094996811503)
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(662322200633811503)
,p_list_item_display_sequence=>60
,p_list_item_link_text=>'Reports'
,p_list_item_link_target=>'f?p=&APP_ID.:26:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'fa-table'
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(1409116797706823675)
,p_list_item_display_sequence=>70
,p_list_item_link_text=>'Sales by Category'
,p_list_item_link_target=>'f?p=&APP_ID.:16:&SESSION.::&DEBUG.:RP,16:::'
,p_parent_list_item_id=>wwv_flow_api.id(662322200633811503)
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(1409116961950825761)
,p_list_item_display_sequence=>80
,p_list_item_link_text=>'Sales by Month'
,p_list_item_link_target=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.:RP,5:::'
,p_parent_list_item_id=>wwv_flow_api.id(662322200633811503)
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(1409117317633827917)
,p_list_item_display_sequence=>90
,p_list_item_link_text=>'Sales by Product'
,p_list_item_link_target=>'f?p=&APP_ID.:27:&SESSION.::&DEBUG.:RP,27:::'
,p_parent_list_item_id=>wwv_flow_api.id(662322200633811503)
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(1409117433030830693)
,p_list_item_display_sequence=>100
,p_list_item_link_text=>'Sales by State'
,p_list_item_link_target=>'f?p=&APP_ID.:31:&SESSION.::&DEBUG.:RP,31:::'
,p_parent_list_item_id=>wwv_flow_api.id(662322200633811503)
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(1409114761266811745)
,p_list_item_display_sequence=>110
,p_list_item_link_text=>'Customer Map'
,p_list_item_link_target=>'f?p=&APP_ID.:13:&SESSION.::&DEBUG.:RP,13:::'
,p_list_item_disp_cond_type=>'NEVER'
,p_parent_list_item_id=>wwv_flow_api.id(662322200633811503)
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(1409116601217819790)
,p_list_item_display_sequence=>120
,p_list_item_link_text=>'Product Order Tree'
,p_list_item_link_target=>'f?p=&APP_ID.:19:&SESSION.::&DEBUG.:RP,19:::'
,p_parent_list_item_id=>wwv_flow_api.id(662322200633811503)
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(1409115938284814576)
,p_list_item_display_sequence=>130
,p_list_item_link_text=>'Customer Orders'
,p_list_item_link_target=>'f?p=&APP_ID.:17:&SESSION.::&DEBUG.:RP,17:::'
,p_parent_list_item_id=>wwv_flow_api.id(662322200633811503)
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(1409117721805833378)
,p_list_item_display_sequence=>140
,p_list_item_link_text=>'Tags'
,p_list_item_link_target=>'f?p=&APP_ID.:28:&SESSION.::&DEBUG.:RP,28:::'
,p_parent_list_item_id=>wwv_flow_api.id(662322200633811503)
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(662322329448811503)
,p_list_item_display_sequence=>150
,p_list_item_link_text=>'Administration'
,p_list_item_link_target=>'f?p=&APP_ID.:33:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'fa-gear'
,p_list_item_current_type=>'TARGET_PAGE'
);
end;
/
