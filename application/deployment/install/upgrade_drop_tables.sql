prompt --application/deployment/install/upgrade_drop_tables
begin
wwv_flow_api.create_install_script(
 p_id=>wwv_flow_api.id(7739459196081461890)
,p_install_id=>wwv_flow_api.id(7182105393450777194)
,p_name=>'Drop Tables'
,p_sequence=>10
,p_script_type=>'UPGRADE'
,p_script_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'drop sequence DEMO_CUST_SEQ;',
'drop sequence DEMO_ORDER_ITEMS_SEQ;',
'drop sequence DEMO_ORD_SEQ;',
'drop sequence DEMO_PROD_SEQ;',
'',
'drop table demo_tags;',
'drop table demo_tags_type_sum;',
'drop table demo_tags_sum;',
'drop table demo_order_items;',
'drop table demo_product_info;',
'drop table demo_states;',
'drop table demo_orders;',
'drop table demo_customers;',
'drop table demo_constraint_lookup;',
'',
'drop package sample_pkg;',
'drop package sample_data_pkg;',
''))
);
end;
/
