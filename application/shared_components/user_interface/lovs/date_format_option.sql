prompt --application/shared_components/user_interface/lovs/date_format_option
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(2186786625571681285)
,p_lov_name=>'DATE_FORMAT_OPTION'
,p_lov_query=>'.'||wwv_flow_api.id(2186786625571681285)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2186786982854681289)
,p_lov_disp_sequence=>10
,p_lov_disp_value=>'Yes'
,p_lov_return_value=>'Y'
);
end;
/
