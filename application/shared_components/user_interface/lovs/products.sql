prompt --application/shared_components/user_interface/lovs/products
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(7477726306730219365)
,p_lov_name=>'PRODUCTS'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select product_name d, product_id r from demo_product_info',
'order by 1'))
);
end;
/
