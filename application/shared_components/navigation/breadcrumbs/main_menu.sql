prompt --application/shared_components/navigation/breadcrumbs/main_menu
begin
wwv_flow_api.create_menu(
 p_id=>wwv_flow_api.id(7475775023920945880)
,p_name=>'Main Menu'
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(944989872376096249)
,p_parent_id=>0
,p_short_name=>'Administration'
,p_link=>'f?p=&APP_ID.:33:&SESSION.::&DEBUG.:::'
,p_page_id=>33
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(945673086453540508)
,p_parent_id=>wwv_flow_api.id(944989872376096249)
,p_short_name=>'Manage States'
,p_link=>'f?p=&APP_ID.:35:&SESSION.::&DEBUG.:::'
,p_page_id=>35
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(1856541525185624192)
,p_parent_id=>wwv_flow_api.id(944989872376096249)
,p_short_name=>'Application Theme Style'
,p_link=>'f?p=&APP_ID.:9:&SESSION.'
,p_page_id=>9
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(2003960991739134506)
,p_parent_id=>wwv_flow_api.id(944989872376096249)
,p_short_name=>'Manage Sample Data'
,p_link=>'f?p=&APP_ID.:25:&SESSION.'
,p_page_id=>25
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(4120362054876850903)
,p_parent_id=>wwv_flow_api.id(7475778819202953992)
,p_short_name=>'Help'
,p_link=>'f?p=&FLOW_ID.:15:&SESSION.'
,p_page_id=>15
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(4120392430864324914)
,p_parent_id=>0
,p_short_name=>'Reports'
,p_link=>'f?p=&APP_ID.:26:&SESSION.::&DEBUG.:::'
,p_page_id=>26
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(4120396756664400758)
,p_parent_id=>wwv_flow_api.id(4120392430864324914)
,p_short_name=>'Sales by Product'
,p_link=>'f?p=&FLOW_ID.:27:&SESSION.'
,p_page_id=>27
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(4120418133726107201)
,p_parent_id=>wwv_flow_api.id(4120392430864324914)
,p_short_name=>'Tags'
,p_link=>'f?p=&FLOW_ID.:28:&SESSION.'
,p_page_id=>28
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(4121275535438721905)
,p_parent_id=>wwv_flow_api.id(7475778819202953992)
,p_short_name=>'Search Results'
,p_link=>'f?p=&FLOW_ID.:30:&SESSION.'
,p_page_id=>30
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(4121653453624402591)
,p_parent_id=>wwv_flow_api.id(4120392430864324914)
,p_short_name=>'Sales by State'
,p_link=>'f?p=&FLOW_ID.:31:&SESSION.'
,p_page_id=>31
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(7475778819202953992)
,p_parent_id=>0
,p_short_name=>'Home'
,p_link=>'f?p=&APP_ID.:1:&SESSION.::&DEBUG.:::'
,p_page_id=>1
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(7475834316217312638)
,p_parent_id=>0
,p_short_name=>'Customers'
,p_link=>'f?p=&APP_ID.:2:&SESSION.::&DEBUG.:::'
,p_page_id=>2
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(7475839136088327843)
,p_parent_id=>wwv_flow_api.id(7475834316217312638)
,p_short_name=>'Customer Details'
,p_link=>'f?p=&APP_ID.:7:&SESSION.::&DEBUG.:::'
,p_page_id=>7
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(7476160518837202878)
,p_parent_id=>0
,p_short_name=>'Products'
,p_link=>'f?p=&APP_ID.:3:&SESSION.::&DEBUG.:::'
,p_page_id=>3
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(7476160825070204708)
,p_parent_id=>wwv_flow_api.id(7476160518837202878)
,p_short_name=>'Product Details'
,p_link=>'f?p=&APP_ID.:6:&SESSION.::&DEBUG.:::'
,p_page_id=>6
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(7476279006931540020)
,p_parent_id=>0
,p_short_name=>'Orders'
,p_link=>'f?p=&APP_ID.:4:&SESSION.::&DEBUG.:::'
,p_page_id=>4
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(7476887206388418455)
,p_parent_id=>wwv_flow_api.id(7476279006931540020)
,p_short_name=>'Order Details'
,p_link=>'f?p=&APP_ID.:29:&SESSION.::&DEBUG.:::'
,p_page_id=>29
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(7492743028669903362)
,p_parent_id=>wwv_flow_api.id(7476279006931540020)
,p_short_name=>'Enter New Order'
,p_link=>'f?p=&APP_ID.:11:&SESSION.::&DEBUG.:::'
,p_page_id=>11
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(7500829910033409183)
,p_parent_id=>wwv_flow_api.id(7476279006931540020)
,p_short_name=>'Order Summary'
,p_link=>'f?p=&APP_ID.:14:&SESSION.::&DEBUG.:::'
,p_page_id=>14
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(7504122025701925387)
,p_parent_id=>wwv_flow_api.id(4120392430864324914)
,p_short_name=>'Sales by Month'
,p_link=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.:::'
,p_page_id=>5
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(7504125018906932874)
,p_parent_id=>wwv_flow_api.id(4120392430864324914)
,p_short_name=>'Sales by Category'
,p_link=>'f?p=&APP_ID.:16:&SESSION.::&DEBUG.:::'
,p_page_id=>16
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(7504156420133046807)
,p_parent_id=>wwv_flow_api.id(4120392430864324914)
,p_short_name=>'Customer Orders'
,p_link=>'f?p=&APP_ID.:17:&SESSION.::&DEBUG.:::'
,p_page_id=>17
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(7518856709953877671)
,p_parent_id=>wwv_flow_api.id(4120392430864324914)
,p_short_name=>'Customer Map'
,p_link=>'f?p=&APP_ID.:13:&SESSION.::&DEBUG.:::'
,p_page_id=>13
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(7518864883266005958)
,p_parent_id=>wwv_flow_api.id(4120392430864324914)
,p_short_name=>'Product Order Tree'
,p_link=>'f?p=&APP_ID.:19:&SESSION.::&DEBUG.:::'
,p_page_id=>19
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(7781269397628858582)
,p_parent_id=>wwv_flow_api.id(7476279006931540020)
,p_short_name=>'Orders Calendar'
,p_link=>'f?p=&APP_ID.:10:&SESSION.::&DEBUG.:::'
,p_page_id=>10
);
end;
/
