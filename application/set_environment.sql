prompt --application/set_environment
set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_190100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2019.03.31'
,p_release=>'19.1.0.00.15'
,p_default_workspace_id=>1260514089020361
,p_default_application_id=>100
,p_default_owner=>'DEV'
);
end;
/
 
prompt APPLICATION 100 - Sample Database Application
--
-- Application Export:
--   Application:     100
--   Name:            Sample Database Application
--   Date and Time:   04:58 Monday January 13, 2020
--   Exported By:     3.91.43.76
--   Flashback:       0
--   Export Type:     Application Export
--   Version:         19.1.0.00.15
--   Instance ID:     250157796364266
--

-- Application Statistics:
--   Pages:                     44
--     Items:                  107
--     Computations:             8
--     Validations:             16
--     Processes:               40
--     Regions:                 98
--     Buttons:                 69
--     Dynamic Actions:         32
--   Shared Components:
--     Logic:
--       Items:                  7
--       Computations:           3
--       App Settings:           1
--       Build Options:          3
--       Data Loading:           1
--     Navigation:
--       Parent Tabs:            1
--       Lists:                 12
--       Breadcrumbs:            1
--         Entries:             25
--       NavBar Entries:         4
--     Security:
--       Authentication:         1
--     User Interface:
--       Themes:                 1
--       Templates:
--         Page:                 9
--         Region:              19
--         Label:                7
--         List:                12
--         Popup LOV:            1
--         Calendar:             1
--         Breadcrumb:           1
--         Button:               3
--         Report:              10
--       LOVs:                  18
--       Shortcuts:              2
--       Plug-ins:               5
--     Globalization:
--       Messages:               4
--     Reports:
--     E-Mail:
--   Supporting Objects:  Included
--     Install scripts:          9

